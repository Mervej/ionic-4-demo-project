import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.page.html',
  styleUrls: ['./demo2.page.scss'],
})
export class Demo2Page implements OnInit {

	demoValue = null;

  	constructor(private activatedRoute: ActivatedRoute) { }

  	ngOnInit() {
  }

}
