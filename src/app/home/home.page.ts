import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestApiService } from '../rest-api.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
	id: string;
  	password: string;	
  	url = 'https://gitlab.com/api/v4/projects/5703234';

  	constructor(private activatedRoute: ActivatedRoute, private restProvider: RestApiService) {
  	}

  	Submitted(userId :string, password :string ){
  		console.log(this.url);
  		this.restProvider.getData(this.url)
	      .then(data => {
	      	console.log("data is "+data);
	      })
	      .catch ((error)=>{
	      	console.log("error while fetching "+error);
	      });

  	}
}
